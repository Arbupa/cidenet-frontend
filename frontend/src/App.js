import { MyTable } from "./components/MyTable";

function App() {
  return (
    <div>
      <MyTable />
    </div>
  );
}

export default App;
