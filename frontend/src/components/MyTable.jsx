import React, { useEffect, useState } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Swal from "sweetalert2";
import axios from "axios";

export const MyTable = () => {
  const [data, setData] = useState([]);
  const emptyEmployee = {
    _id: "",
    admission_date: "",
    area: "",
    country_working: "",
    domain: "",
    email: "",
    first_last_name: "",
    first_name: "",
    id_number: "",
    id_type: "",
    other_names: "",
    register_date: "",
    second_last_name: "",
    state: "",
    update_date: "",
  };
  const [reloadData, setReloadData] = useState(false);
  const [modalInsert, setModalInsert] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const [employeeSelected, setEmployeeSelected] = useState(emptyEmployee);

  // useEffect to collect data first on render, and at every change on "reloadData"
  useEffect(() => {
    axios.get("http://localhost:9000/employees").then((response) => {
      setData(response.data);
    });
  }, [reloadData]);

  // function to catch the values of each input based on their names
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEmployeeSelected((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  // function to send data to backend and create an employee
  const createEmployee = async (employee) => {
    console.log("crear employee: \n", employee);
    try {
      await axios
        .post("http://localhost:9000/employee", employee)
        .then((response) => {
          // console.log(response.data)
          Swal.fire({
            icon: "success",
            title: "¡El empleado se registró exitosamente!",
            showConfirmButton: false,
            timer: 1500,
          });
        });
      setReloadData(!reloadData);
    } catch (error) {
      console.log(error);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "¡Algo salió mal y no se pudo crear!",
        footer: "Revisa bien la información que envías e inténtalo de nuevo.",
      });
    }
    setModalInsert(false);
  };

  // function to send data to backend and edit an employee
  const editEmployee = async (id, employee) => {
    console.log("editar employee: \n", employee);
    try {
      await axios
        .put(`http://localhost:9000/employee/${id}`, employee)
        .then((response) => {
          // console.log(response);
          Swal.fire({
            icon: "success",
            title: `¡El empleado ${employee.first_name} ${employee.first_last_name} se editó correctamente!`,
            showConfirmButton: false,
            timer: 1500,
          });
        });
      setReloadData(!reloadData);
      setModalEdit(false);
    } catch (error) {
      console.log(error);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "¡Algo salió mal y no se pudo editar!",
        footer: "Revisa bien la información que envías e inténtalo de nuevo.",
      });
    }
  };

  // function to send data to backend and delete an employee
  const deleteEmployee = async (idEmployee) => {
    try {
      await axios
        .delete(`http://localhost:9000/employee/${idEmployee}`)
        .then((response) => {
          console.log(response.data);
          setReloadData(!reloadData);
          Swal.fire({
            icon: "success",
            title: "¡El empleado se eliminó correctamente!",
            showConfirmButton: false,
            timer: 1500,
          });
        });
    } catch (error) {
      console.log(error);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "¡Algo salió mal y no se pudo eliminar!",
        footer: "Trabajamos en ello...",
      });
    }
  };

  // open delete modal to confirm/cancel operation delete of an employee
  const deleteConfirmation = (employee) => {
    Swal.fire({
      title: `¿Seguro que quieres eliminar a: ${employee.first_name} ${employee.first_last_name}?`,
      text: "¡¡No podrás deshacer esta acción!!",
      showDenyButton: true,
      confirmButtonText: "Si",
      denyButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteEmployee(employee._id);
      } else if (result.isDenied) {
        Swal.fire("No se realizaron cambios", "", "info");
      }
    });
  };

  // function to update the "employeeSelected" state
  // and have the info of the user selected.
  // also open the respective modal
  const selectEmployee = (employee, nameAction) => {
    setEmployeeSelected(employee);

    if (nameAction === "insert") {
      setModalInsert(true);
    }

    if (nameAction === "edit") {
      setModalEdit(true);
    }
    if (nameAction === "delete") {
      deleteConfirmation(employee);
    }
  };

  return (
    <div className="container">
      <h2 align="center">Cidenet Empleados</h2>
      <button
        className="btn btn-success mb-2"
        onClick={() => selectEmployee(emptyEmployee, "insert")}
      >
        Agregar Empleado
      </button>
      <div align="center">
        <table
          style={{
            fontSize: "1vw",
            textAlign: "center",
            fontFamily: "unset",
          }}
          className="table table-info table-striped table-bordered table-hover"
        >
          <thead>
            <tr className="table-dark">
              <th>Primer nombre</th>
              <th>Primer Apellido</th>
              <th>Segundo Apellido</th>
              <th>Otros nombres</th>
              <th>Tipo Identificación</th>
              <th>Num. Identificación</th>
              <th>País</th>
              <th>Correo</th>
              <th>Estado</th>
              <th>Área</th>
              <th>Fecha Ingreso</th>
              <th>Fecha Registro</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.map((employee) => (
                <tr>
                  <td key={employee.first_name}>{employee.first_name}</td>
                  <td key={employee.first_last_name}>
                    {employee.first_last_name}
                  </td>
                  <td key={employee.second_last_name}>
                    {employee.second_last_name}
                  </td>
                  <td key={employee.other_names}>{employee.other_names}</td>
                  <td key={employee.id_type}>{employee.id_type}</td>
                  <td key={employee.id_number}>{employee.id_number}</td>
                  <td key={employee.country_working}>
                    {employee.country_working}
                  </td>
                  <td key={employee.email}>{employee.email}</td>
                  <td key={employee.state}>{employee.state}</td>
                  <td key={employee.area}>{employee.area}</td>
                  <td key={employee.admission_date}>
                    {employee.admission_date}
                  </td>
                  <td key={employee.register_date}>{employee.register_date}</td>
                  <td key={employee._id}>
                    <button
                      className="btn btn-primary"
                      onClick={() => selectEmployee(employee, "edit")}
                    >
                      Editar
                    </button>{" "}
                    <button
                      className="btn btn-danger"
                      onClick={() => selectEmployee(employee, "delete")}
                    >
                      Eliminar
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>

        <div>
          {/* modalEdit handle the state of this modal to open/close it */}
          <Modal isOpen={modalEdit}>
            <ModalHeader>
              <h3>Editar Empleado</h3>
            </ModalHeader>
            <ModalBody>
              <div className="form-group">
                <label htmlFor="">ID Base de datos</label>
                <input
                  className="form-control"
                  type="text"
                  name="id"
                  readOnly
                  value={employeeSelected._id}
                />
                <br />
                <label htmlFor="">Primer Nombre (máx. 20 caracteres)</label>
                <input
                  className="form-control"
                  type="text"
                  name="first_name"
                  value={employeeSelected && employeeSelected.first_name}
                  // adding validations also in frontend
                  //   value={
                  //     companySelected.name.length < 51
                  //       ? companySelect && companySelected.name
                  //       : ""
                  //   }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Primer Apellido (máx. 20 caracteres)</label>
                <textarea
                  className="form-control"
                  type="text"
                  name="first_last_name"
                  value={employeeSelected && employeeSelected.first_last_name}
                  // adding validations also in frontend
                  //   value={
                  //     companySelected.description.length < 101
                  //       ? companySelect && companySelected.description
                  //       : ""
                  //   }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Segundo Apellido (máx. 20 caracteres)</label>
                <input
                  className="form-control"
                  type="text"
                  name="second_last_name"
                  value={employeeSelected && employeeSelected.second_last_name}
                  // adding validations also in frontend
                  //   value={
                  //     companySelected.symbol.length < 11
                  //       ? companySelect && companySelected.symbol
                  //       : ""
                  //   }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Otros Nombres (máx. 50 caracteres)</label>
                <input
                  className="form-control"
                  type="text"
                  name="other_names"
                  value={employeeSelected && employeeSelected.other_names}
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">
                  Tipo de Identificacion (máx. 20 caracteres)
                </label>
                <select
                  class="form-select form-select-lg mb-3"
                  aria-label=".form-select-lg example"
                  name="id_type"
                  onChange={handleInputChange}
                  // value={employeeSelected && employeeSelected.country_working}
                >
                  <option selected>
                    {employeeSelected && employeeSelected.id_type}
                  </option>
                  <option value="Cedula de Ciudadania">
                    Cédula de Ciudadanía
                  </option>
                  <option value="Cedula de Extranjeria">
                    Cédula de Extranjería
                  </option>
                  <option value="Pasaporte">Pasaporte</option>
                  <option value="Permiso Especial">Permiso Especial</option>
                </select>
                <br />
                <label htmlFor="">
                  Número de identificación (máx. 20 caracteres)
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="id_number"
                  value={employeeSelected && employeeSelected.id_number}
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">País</label>
                <select
                  class="form-select form-select-lg mb-3"
                  aria-label=".form-select-lg example"
                  name="country_working"
                  onChange={handleInputChange}
                  // value={employeeSelected && employeeSelected.country_working}
                >
                  <option selected>
                    {employeeSelected && employeeSelected.country_working}
                  </option>
                  <option value="Colombia">Colombia</option>
                  <option value="Estados Unidos">Estados Unidos</option>
                </select>

                <br />
                <label htmlFor="">Correo </label>
                <input
                  className="form-control"
                  type="text"
                  name="email"
                  readOnly
                  value={employeeSelected && employeeSelected.email}
                />
                <br />
                <label htmlFor="">Estado</label>
                <input
                  className="form-control"
                  type="text"
                  name="state"
                  value={employeeSelected && employeeSelected.state}
                  readOnly
                />
                <br />
                <label htmlFor="">Área</label>
                <select
                  class="form-select form-select-lg mb-4"
                  aria-label=".form-select-lg example"
                  name="area"
                  onChange={handleInputChange}
                  // value={employeeSelected && employeeSelected.country_working}
                >
                  <option selected>
                    {employeeSelected && employeeSelected.area}
                  </option>
                  <option value="Administracion">Administración</option>
                  <option value="Financiera">Financiera</option>
                  <option value="Compras">Compras</option>
                  <option value="Infraestructura">Infraestructura</option>
                  <option value="Operacion">Operación</option>
                  <option value="Talento Humano">Talento Humano</option>
                  <option value="Servicios Varios">Servicios Varios</option>
                </select>
                <br />
                <label htmlFor="">Fecha Ingreso</label>
                <input
                  className="form-control"
                  type="text"
                  name="admission_date"
                  value={employeeSelected && employeeSelected.admission_date}
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Última fecha de Edición</label>
                <input
                  className="form-control"
                  type="text"
                  name="update_date"
                  readOnly
                  value={employeeSelected && employeeSelected.update_date}
                />
                <br />
              </div>
            </ModalBody>
            <ModalFooter>
              <button
                className="btn btn-lg btn-info"
                onClick={async () =>
                  await editEmployee(employeeSelected._id, {
                    first_name: employeeSelected.first_name.toUpperCase(),
                    first_last_name:
                      employeeSelected.first_last_name.toUpperCase(),
                    second_last_name:
                      employeeSelected.second_last_name.toUpperCase(),
                    other_names: employeeSelected.other_names.toUpperCase(),
                    id_type: employeeSelected.id_type,
                    id_number: employeeSelected.id_number,
                    country_working: employeeSelected.country_working,
                    area: employeeSelected.area,
                    admission_date: employeeSelected.admission_date,
                  })
                }
              >
                Actualizar
              </button>
              <button
                // if Cancel option is clicked hide ModalInsert
                onClick={() => setModalEdit(false)}
                className="btn btn-lg btn-danger"
              >
                Cancelar
              </button>
            </ModalFooter>
          </Modal>

          {/* modalInsert handle the state of this modal to open/close it */}
          <Modal isOpen={modalInsert}>
            <ModalHeader>
              <h3>Crear Empleado</h3>
            </ModalHeader>
            <ModalBody>
              <div className="form-group">
                <label htmlFor="">Primer Nombre (20 caracteres)</label>
                <input
                  className="form-control"
                  type="text"
                  name="first_name"
                  value={
                    employeeSelected.first_name.length < 21
                      ? employeeSelected && employeeSelected.first_name
                      : ""
                  }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Primer Apellido (máx. 20 caracteres)</label>
                <textarea
                  className="form-control"
                  type="text"
                  name="first_last_name"
                  value={
                    employeeSelected.first_last_name.length < 21
                      ? employeeSelected && employeeSelected.first_last_name
                      : ""
                  }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Segundo Apellido (máx. 20 caracteres)</label>
                <input
                  className="form-control"
                  type="text"
                  name="second_last_name"
                  value={
                    employeeSelected.second_last_name.length < 21
                      ? employeeSelected && employeeSelected.second_last_name
                      : ""
                  }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Otros Nombres (máx. 50 caracteres)</label>
                <input
                  className="form-control"
                  type="text"
                  name="other_names"
                  value={
                    employeeSelected.other_names.length < 51
                      ? employeeSelected && employeeSelected.other_names
                      : ""
                  }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">Tipo de Identificacion</label>
                <select
                  class="form-select form-select-lg mb-3"
                  aria-label=".form-select-lg example"
                  onChange={handleInputChange}
                  name="id_type"
                  // value={employeeSelected && employeeSelected.country_working}
                >
                  <option selected value="">
                    Elige la opción
                  </option>
                  <option value="Cedula de Ciudadania">
                    Cédula de Ciudadanía
                  </option>
                  <option value="Cedula de Extranjeria">
                    Cédula de Extranjería
                  </option>
                  <option value="Pasaporte">Pasaporte</option>
                  <option value="Permiso Especial">Permiso Especial</option>
                </select>
                <br />
                <label htmlFor="">
                  Número de identificación (máx. 20 caracteres)
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="id_number"
                  value={
                    employeeSelected.id_number.length < 21
                      ? employeeSelected && employeeSelected.id_number
                      : ""
                  }
                  onChange={handleInputChange}
                />
                <br />
                <label htmlFor="">País</label>
                <select
                  class="form-select form-select-lg mb-3"
                  aria-label=".form-select-lg example"
                  name="country_working"
                  // value={employeeSelected && employeeSelected.country_working}
                  onChange={handleInputChange}
                >
                  <option selected value="">
                    Elige la opción
                  </option>
                  <option value="Colombia">Colombia</option>
                  <option value="Estados Unidos">Estados Unidos</option>
                </select>
                <br />

                <label htmlFor="">Estado</label>
                <input
                  className="form-control"
                  type="text"
                  name="state"
                  value="Activo"
                  readOnly
                />
                <br />
                <label htmlFor="">Área</label>
                <select
                  class="form-select form-select-lg mb-4"
                  aria-label=".form-select-lg example"
                  // value={employeeSelected && employeeSelected.country_working}
                  name="area"
                  onChange={handleInputChange}
                >
                  <option selected value="">
                    Elige la opción
                  </option>
                  <option value="Administracion">Administración</option>
                  <option value="Financiera">Financiera</option>
                  <option value="Compras">Compras</option>
                  <option value="Infraestructura">Infraestructura</option>
                  <option value="Operacion">Operación</option>
                  <option value="Talento Humano">Talento Humano</option>
                  <option value="Servicios Varios">Servicios Varios</option>
                </select>
                <br />
                <label htmlFor="">Fecha Ingreso</label>
                <input
                  className="form-control"
                  placeholder="DD/MM/YYYY HH:mm:ss"
                  type="text"
                  name="admission_date"
                  value={employeeSelected && employeeSelected.admission_date}
                  onChange={handleInputChange}
                />
                <br />
              </div>
            </ModalBody>
            <ModalFooter>
              <button
                className="btn btn-lg btn-primary"
                onClick={async () =>
                  await createEmployee({
                    first_name: employeeSelected.first_name.toUpperCase(),
                    first_last_name:
                      employeeSelected.first_last_name.toUpperCase(),
                    second_last_name:
                      employeeSelected.second_last_name.toUpperCase(),
                    other_names: employeeSelected.other_names.toUpperCase(),
                    id_type: employeeSelected.id_type,
                    id_number: employeeSelected.id_number,
                    country_working: employeeSelected.country_working,
                    area: employeeSelected.area,
                    admission_date: employeeSelected.admission_date,
                  })
                }
              >
                Insertar
              </button>
              <button
                onClick={() => setModalInsert(false)}
                className="btn btn-lg btn-danger"
              >
                Cancelar
              </button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    </div>
  );
};
