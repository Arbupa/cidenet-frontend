## Requerimientos de instalación:

- instalar Node.js

#### Comandos:

`npm install`

Verificar que la instalación se haya hecho de manera correcta, si no
entonces ir a "requirements.txt" y ejecutar manualmente esos comandos desde la consola.

Una vez que se instalaron todas las dependencias simplemente ejecutar dentro de la ruta \frontend\: <br>

`npm start`

## IMPORTANTE:

**Tener ejecutando primero el backend, de lo contrario la aplicación no funcionará de la manera adecuada.**
